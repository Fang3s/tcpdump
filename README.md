#tcpdump

## 各模块版本号
1. tcpdump-4.7.4	http://www.tcpdump.org/
2. libpcap-1.6.2	http://www.tcpdump.org/
3. openssl			https://github.com/eighthave/openssl-android

###github mirrored from google
https://github.com/android/platform_external_tcpdump mirrored from https://android.googlesource.com/platform/external/tcpdump.git  
https://github.com/android/platform_external_libpcap mirrored from https://android.googlesource.com/platform/external/libpcap.git  
https://github.com/android/platform_external_openssl mirrored from https://android.googlesource.com/platform/external/openssl.git  
 
###openssl
http://www.openssl.org/source/  
5262089 Mar 19 13:53:47 2015 openssl-1.0.2-latest.tar.gz  
5262089 Mar 19 13:53:47 2015 openssl-1.0.2a.tar.gz (MD5) (SHA1) (PGP sign)  [LATEST]
 
## 编译

全部编译完需要等好几以致十几分钟，所以在jni\Application.mk文件中的APP_ABI = armeabi armeabi-v7a x86会编译几个版本，按需修改。

注意：就我编译而言，有一些错误，最好看一下，不然编译出错浪费时间。

 1. 对于[1]和[2]，编译路径（工程路径D:\Developer\workspace\ADemo）需要根据工程位置修改或者直接和这里相同路径。
   本来想用语法解决，可是例如NDK_PROJECT_PATH := $(LOCAL_PATH)/jni/openssl我用ls来查看结果，正确，编译时就不正确，目前不明。
 2. 对于[3]，缺少文件已经补上。
 3. 对于[4]，armeabi和x86版，目前不明的编译错误，需提前注意。

[1]

文件D:\Developer\workspace\ADemo\jni\Android.mk文件修改位置：

```makefile
LOCAL_C_INCLUDES += D:\Developer\workspace\ADemo\jni\libpcap\
	D:\Developer\workspace\ADemo\jni\openssl\include
	
tcpdump_CSRC2 = print-loopback.c print-aoe.c print-m3ua.c print-smtp.c print-http.c \
	print-ftp.c print-rtsp.c print-geneve.c print-ahcp.c print-pktap.c	
```

[2]

文件D:\Developer\workspace\ADemo\jni\openssl\Android.mk添加如下：

```makefile
NDK_PROJECT_PATH := $(MY_WORK_DIR_PATH)/jni/openssl
NDK_PROJECT_PATH := D:\Developer\workspace\ADemo\jni\openssl
$(info >>>>>>>[$(TARGET_ARCH)][$(LOCAL_PATH)]NDK_PROJECT_PATH = $(NDK_PROJECT_PATH))
```

[3]
			
	其实这些都是执行脚本自动生成的
	
 (1) libpcap模块从Android源/extern/libpcap摘取如下文件：
 
		#include "version.h"
		static const char pcap_version_string[] = "libpcap version 1.0.2";
		
		version.c

		tokdefs.h
		grammar.c
		bpf_filter.c
 (2) tcpdump模块从Android源/extern/tcpdump摘取如下文件：
 
		version.c

[4]

如果在编译pcap-usb-linux.c出现如下错误

```
[armeabi] Compile thumb  : pcap <= pcap-linux.c
D:/Developer/workspace/ADemo/jni/libpcap/pcap-linux.c:113:0: warning: "_GNU_SOURCE" redefined [enabled by default]
<command-line>:0:0: note: this is the location of the previous definition
[armeabi] Compile thumb  : pcap <= pcap-usb-linux.c
In file included from D:/Developer/ndk/android-ndk-r10c/platforms/android-21/arch-arm/usr/include/stdlib.h:34:0,
                 from D:/Developer/workspace/ADemo/jni/libpcap/pcap-usb-linux.c:54:
D:/Developer/ndk/android-ndk-r10c/platforms/android-21/arch-arm/usr/include/string.h:80:15: error: expected declaration specifiers or '...' before '(' token
D:/Developer/ndk/android-ndk-r10c/platforms/android-21/arch-arm/usr/include/string.h:80:15: error: expected declaration specifiers or '...' before '(' token
D:/Developer/ndk/android-ndk-r10c/platforms/android-21/arch-arm/usr/include/string.h:80:15: error: expected declaration specifiers or '...' before '(' token
D:/Developer/ndk/android-ndk-r10c/platforms/android-21/arch-arm/usr/include/string.h:80:15: error: expected ')' before ',' token
make.exe: *** [obj/local/armeabi/objs/pcap/pcap-usb-linux.o] Error 1
```

可以对所在NDK库，如
文件D:\Developer\ndk\android-ndk-r10c\platforms\android-21\arch-arm\usr\include\string.h注释掉如下：
```c
//extern size_t strlcpy(char* __restrict, const char* __restrict, size_t);
```
目前不明，strlcpy为啥会出现如此错误


##Snippet

```makefile
$(info >>>>>>>[$(TARGET_ARCH)]LOCAL_PATH = $(LOCAL_PATH), APP_PROJECT_PATH = $(APP_PROJECT_PATH))

MY_WORK_DIR_PATH := $(shell pwd)
$(info >>>>>>>[$(TARGET_ARCH)][$(MY_WORK_DIR_PATH)]$(MY_WORK_DIR_PATH)/../../)
```

##题外

今天把手机更新了一下，竟然一下从4.1更到了5.0.2LRX22G

 1. 发现tcpdump不行了。
 2. getRuningTasks也不好使了，需要新权限REAL_GET_TASKS。其实旧的GET_TASKS也行，但是需要满足isUidPrivileged(callingUid)。
   即使添加了新权限，获取不到其他用户的情况，只有自己和Home的。
	
```java
//ActivityManagerService.java
public List<ActivityManager.RecentTaskInfo> getRecentTasks(int maxNum, int flags, int userId) {
	
	//...
	
	// Only add calling user or related users recent tasks
	if (!includedUsers.contains(Integer.valueOf(tr.userId))) {
		if (DEBUG_RECENTS) Slog.d(TAG, "Skipping, not user: " + tr);
		continue;
	}
	
	//...
	
	// If the caller doesn't have the GET_TASKS permission, then only
    // allow them to see a small subset of tasks -- their own and home.
	
	//...
}
```