#NDK_TOOLCHAIN_VERSION=4.4.3
#APP_PROJECT_PATH := $(shell pwd)
#APP_BUILD_SCRIPT := $(APP_PROJECT_PATH)/Android.mk
#$(info >>>>>>>[$(TARGET_ARCH)][$(call my-dir)]APP_PROJECT_PATH = $(APP_PROJECT_PATH)

#APP_ABI：编译架构，so文件都会打在apk中，而且会依据系统CPU架构进行安装。如下两种方法：
#方法1、创建Application.mk文件，则在该文件添加，APP_ABI := armeabi armeabi-v7a x86
#方法2、在ndk-build 参数中添加，APP_ABI="armeabi armeabi-v7a x86"
#比如：
#    为了在ARMv7的设备上支持硬件FPU指令。可以使用  APP_ABI := armeabi-v7a 
#    或者为了支持IA-32指令集，可以使用      APP_ABI := x86 
#    或者为了同时支持这三种，可以使用       APP_ABI := armeabi armeabi-v7a x86

APP_ABI = armeabi armeabi-v7a x86

