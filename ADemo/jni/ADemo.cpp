#include <jni.h>

jstring com_fang_ademo_MainActivity_method(JNIEnv *pEnv, jobject o, jstring param)
{

	return pEnv->NewStringUTF("222");
}
/*
 * 由于gMethods[]是一个<名称，函数指针>对照表，在程序执行时，
 * 可多次调用registerNativeMethods()函数来更换本地函数的指针，
 * 从而达到弹性调用本地函数的目的。
 */
static JNINativeMethod gMethods[] =
{
/* name, signature, funcPtr */
{ "method", "(Ljava/lang/String;)Ljava/lang/String;",
		(void*) com_fang_ademo_MainActivity_method }, };

static const char* const className = "com/fang/ademo/MainActivity";

//在native 注册的时候首先保存java的调用方法：以便事件回调而不必每次获取，即在native里调用java对象，方法等。
int register_com_fang_ademo(JNIEnv* pEnv)
{
	jclass clazz;

	//ALOGI(TAG, "Registering %s natives\n", className);
	clazz = pEnv->FindClass(className);
	if (clazz == 0)
	{
		//ALOGE("Native registration unable to find class '%s'\n", className);
		return -1;
	}
	if (pEnv->RegisterNatives(clazz, gMethods, 1) < 0)
	{
		//ALOGE("RegisterNatives failed for '%s'\n", className);
		return -1;
	}
	return 0;
}

// Set some test stuff up.
//Used by WithFramework to register native functions.
//Returns the JNI version on success, -1 on failure.
//Dalvik虚拟机加载C库时，第一件事是调用JNI_OnLoad()函数
extern "C" jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	JNIEnv* env = 0;

	/*JavaVM::GetEnv 原型为 jint (*GetEnv)(JavaVM*, void**, jint);
	 * GetEnv()函数返回的  Jni 环境对每个线程来说是不同的，
	 * 由于Dalvik虚拟机通常是Multi-threading的。每一个线程调用JNI_OnLoad()时，
	 * 所用的JNI Env是不同的，因此我们必须在每次进入函数时都要通过vm->GetEnv重新获取
	 */
	//得到JNI Env
	if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_4) != JNI_OK)
	{
		return JNI_ERR;
	}
	// Get jclass with env->FindClass.
	// Register methods with env->RegisterNatives
	//assert(env != NULL);
	register_com_fang_ademo(env);
	// success -- return valid version number
	return JNI_VERSION_1_4;
}
