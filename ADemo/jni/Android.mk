LOCAL_PATH:= $(call my-dir)
MY_WORK_DIR_PATH := $(shell pwd)

# Include makefiles here. Its important that these includes are done after the main module, explanation below.

# @see http://stackoverflow.com/questions/6942730/android-ndk-how-to-include-android-mk-into-another-android-mk-hierarchical-pro
# @see the above IMPORTANT NOTE of LOCAL_PATH

# create a temp variable with the current path, because it changes after each include
SAVED_LOCAL_PATH := $(LOCAL_PATH)

$(info >>>>>>>[$(TARGET_ARCH)][$(LOCAL_PATH)]before calling,  LOCAL_PATH = $(LOCAL_PATH))
#include $(SAVED_LOCAL_PATH)/instruments/jni/Android.mk
include $(call all-subdir-makefiles)

$(info >>>>>>>[$(TARGET_ARCH)]after calling, LOCAL_PATH = $(LOCAL_PATH))  

#LOCAL_PATH := $(SAVED_LOCAL_PATH)
#$(info >>>>>>>[$(TARGET_ARCH)]after restoring, LOCAL_PATH = $(LOCAL_PATH))  
